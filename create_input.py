# Python code to 
# demonstrate readlines() 
  
#L = ["Geeks\n", "for\n", "Geeks\n"] 
  
# writing to file 
#file1 = open('myfile.txt', 'w') 
#file1.writelines(L) 
#file1.close() 
  
# Using readlines() 
dsids = ["364375","364376","364377","364378","364379","364380","410470","410471"]#,"700040","700041","700042","700043","700044"]
#dsids = ["700040","700041","700042","700043","700044"]
#subcoms = ["mc16a","mc16d","mc16e"]
#dsids = ["410470","410471"]
subcoms = ["mc16a"]
for subcom in subcoms:
    for dsid in dsids:
        file1 = open(subcom+'-leptonveto/'+dsid+'-3.txt', 'r')
        Lines = file1.readlines()
        file2 = open("/pbs/home/y/yahe/work/histogram-maker/input/"+subcom+"-"+dsid+"-leptonveto-3.txt", 'w')
        for line in Lines: 
            if "root://ccxrootdatlas.in2p3.fr" not in line:
                continue
            linesplit = line.split(" ")
            for lines in linesplit:
                if "root://ccxrootdatlas.in2p3.fr" not in lines:
                    continue    
                file2.write(lines+" \n")
        file2.close()

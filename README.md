VZ+jets calibration: Final selection
======================================

This is code to get trees for fit and studies.

The inputs of this code are the data files produced by [VZjetsSF-ATOPNtuple](https://gitlab.cern.ch/yajun/vzjetssf-atopntuple) 

To use this code:

```
git clone https://gitlab.cern.ch/yajun/vzjetssf-finalselection.git
cd vzjetssf-finalselection
source setup.sh
```
If you run with MC simulations:
```
make mc
./mc [input-file] [output-dir] [systematic]
```

If you run with data:
```
make data
./data [input-file] [output-dir] [systematic]
```

```input-file```: a txt file contains the list of inputs.

Several warning lines will appear when making from python. It does not affect the program and I'm trying to solve it. For this moment, please ignore it.

After modify the code,

```
make clean
make [mc/data]
```

For test (UNAL students)
====
- Step0: Create your own branch
- Step1: Compile sucessfully the framework
- Step2: You will find four .txt files in the ```input```. To check the framework:
```
./mc input/mc16a-700042-test.txt ./myout nominal
``` 
If it runs successfully, you will find the output file with name `myout-pTfull-nominal.root`. Now, you can have a look at the output files. We're interested in the *Ztag* branch. Congratulations!

- Step3: Your input files are: `mc16a-700042.txt or mc16d-700042.txt or mc16e-700042`. I'd like to draw your attention that these input files are very large. If you run with the full lists, it costs a lot of CPUs. I suggest to start with `mc16a-700042.txt`.

PS: 700042 is Sherpa 2.2.8 Z->bb samples.


Build CI/CD workflow (for UNAL students)
====
Assuming that you understand what CI/CD is and Why it's useful.

The goal of this project is to complete the CI/CD workflow for this framework. You can start the project from the '.gitlab-ci.yml' file.

In '.gitlab.yml' file, there are already 3 stages:

- Dummy file

- Dictionary

- Build

You're expected to the stages below for this CI/CD:

- run: I wrote something there. But it's in failed state. Try to fix it or modify it.

- plot: Some extra scripts are needed to do the plot (in c++ or python). You should choose which histograms you're going to use.

Signal significance (for UNAL students)
===
- Goal1: Add the event numbers after each selections in the code and make this numbers shown in the CI/CD.
- Goal2: Run dijets samples (dsid: 364700-364712, bfil: 800600,800601) and ttbar samples (410471) and draw the kinematic plots
- Goal3: Calculate the signal significance of of after each selections using different definition of signal significance: (S/sqrt(B),S/sqrt(S+B), asimov significance (https://link.springer.com/content/pdf/10.1140/epjc/s10052-011-1554-0.pdf))
- Goal4: Optimise event selections


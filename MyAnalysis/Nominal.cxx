#define Nominal_cxx
// The class definition in Nominal.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.


// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("Nominal.C")
// root> T->Process("Nominal.C","some options")
// root> T->Process("Nominal.C+")
//
#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "Nominal.h"
#include <TH2.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFileInfo.h>
#include <TFileCollection.h>
#include <math.h>
#include <sstream>

#include <TSystemDirectory.h>
#include <TSystem.h>


void Nominal::Begin(TTree * /*tree*/)
{
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.
  // The tree argument is deprecated (on PROOF 0 is passed).

  TString option = GetOption();

  //std::cout << fChain->GetEntries() << std::endl;

  Btag = new TTree("Ztag","Z tag");
  Btag->Branch("lj1_mass",&lj1_mass);
  Btag->Branch("lj1_pt",&lj1_pt);
  Btag->Branch("lj1_eta",&lj1_eta);
  Btag->Branch("lj1_phi",&lj1_phi);
  Btag->Branch("lj1_truthmass",&lj1_truthmass);
  Btag->Branch("lj1_truthlabel",&lj1_truthlabel);
  Btag->Branch("lj1_truthpt",&lj1_truthpt);
  Btag->Branch("lj1_nVRjets",&lj1_nVRjets);
  Btag->Branch("lj1_VRjet1_id",&lj1_VRj1_id);
  Btag->Branch("lj1_VRjet2_id",&lj1_VRj2_id);
  Btag->Branch("lj1_VRjet3_id",&lj1_VRj3_id);
  Btag->Branch("lj1_XbbDiscriminant_f25",&lj1_XbbDiscriminant_f25);
  Btag->Branch("lj1_isXbb_50",&lj1_isXbb_50);
  Btag->Branch("lj1_isXbb_60",&lj1_isXbb_60);
  Btag->Branch("lj1_isXbb_70",&lj1_isXbb_70);
  Btag->Branch("lj1_isXbb_80",&lj1_isXbb_80);
  Btag->Branch("lj1_D2",&lj1_D2);
  Btag->Branch("lj1_Ntrk",&lj1_Ntrk);

  Btag->Branch("lj2_mass",&lj2_mass);
  Btag->Branch("lj2_pt",&lj2_pt);
  Btag->Branch("lj2_eta",&lj2_eta);
  Btag->Branch("lj2_phi",&lj2_phi);
  Btag->Branch("lj2_truthmass",&lj2_truthmass);
  Btag->Branch("lj2_truthlabel",&lj2_truthlabel);
  Btag->Branch("lj2_truthpt",&lj2_truthpt);
  Btag->Branch("lj2_nVRjets",&lj2_nVRjets);
  Btag->Branch("lj2_VRjet1_id",&lj2_VRj1_id);
  Btag->Branch("lj2_VRjet2_id",&lj2_VRj2_id);
  Btag->Branch("lj2_VRjet3_id",&lj2_VRj3_id);
  Btag->Branch("lj2_XbbDiscriminant_f25",&lj2_XbbDiscriminant_f25);
  Btag->Branch("lj2_isXbb_50",&lj2_isXbb_50);
  Btag->Branch("lj2_isXbb_60",&lj2_isXbb_60);
  Btag->Branch("lj2_isXbb_70",&lj2_isXbb_70);
  Btag->Branch("lj2_isXbb_80",&lj2_isXbb_80);
  Btag->Branch("lj2_D2",&lj2_D2);
  Btag->Branch("lj2_Ntrk",&lj2_Ntrk);

  Btag->Branch("weight",&tot_weight);
  Btag->Branch("MCWeight",&MCWeight);
  Btag->Branch("SampleWeight",&SampleWeight);
  Btag->Branch("PUWeight",&PUWeight);

  Vtag = new TTree("Vtag","V tag");
  Vtag->Branch("nlargeRjets",&NlargeRjets);
  Vtag->Branch("lj1_mass",&lj1_mass);
  Vtag->Branch("lj1_mt",&lj1_mt);
  Vtag->Branch("lj1_pt",&lj1_pt);
  Vtag->Branch("lj1_eta",&lj1_eta);
  Vtag->Branch("lj1_phi",&lj1_phi);
  Vtag->Branch("lj1_truthmass",&lj1_truthmass);
  Vtag->Branch("lj1_truthlabel",&lj1_truthlabel);
  Vtag->Branch("lj1_truthpt",&lj1_truthpt); 
  Vtag->Branch("lj1_D2",&lj1_D2);
  Vtag->Branch("lj1_Ntrk",&lj1_Ntrk);

  Vtag->Branch("lj1_passWD2_50",&lj1_passWD2_50);
  Vtag->Branch("lj1_passWD2_80",&lj1_passWD2_80);
  Vtag->Branch("lj1_passWNtrk_50",&lj1_passWNtrk_50);
  Vtag->Branch("lj1_passWNtrk_80",&lj1_passWNtrk_80);
  Vtag->Branch("lj1_passWMassCut_50",&lj1_passWMassCut_50);
  Vtag->Branch("lj1_passWMassCut_80",&lj1_passWMassCut_80);
  Vtag->Branch("lj1_isWTagged_50",&lj1_isWTagged_50);
  Vtag->Branch("lj1_isWTagged_80",&lj1_isWTagged_80);

  Vtag->Branch("lj1_passZD2_50",&lj1_passZD2_50);
  Vtag->Branch("lj1_passZD2_80",&lj1_passZD2_80);
  Vtag->Branch("lj1_passZNtrk_50",&lj1_passZNtrk_50);
  Vtag->Branch("lj1_passZNtrk_80",&lj1_passZNtrk_80);
  Vtag->Branch("lj1_passZMassCut_50",&lj1_passZMassCut_50);
  Vtag->Branch("lj1_passZMassCut_80",&lj1_passZMassCut_80);
  Vtag->Branch("lj1_isZTagged_50",&lj1_isZTagged_50);
  Vtag->Branch("lj1_isZTagged_80",&lj1_isZTagged_80);

  Vtag->Branch("lj2_mass",&lj2_mass);
  Vtag->Branch("lj2_mt",&lj2_mt);
  Vtag->Branch("lj2_pt",&lj2_pt);
  Vtag->Branch("lj2_eta",&lj2_eta);
  Vtag->Branch("lj2_phi",&lj2_phi);
  Vtag->Branch("lj2_truthmass",&lj2_truthmass);
  Vtag->Branch("lj2_truthlabel",&lj2_truthlabel);
  Vtag->Branch("lj2_truthpt",&lj2_truthpt);
  Vtag->Branch("lj2_D2",&lj2_D2);
  Vtag->Branch("lj2_Ntrk",&lj2_Ntrk);

  Vtag->Branch("lj2_passWD2_50",&lj2_passWD2_50);
  Vtag->Branch("lj2_passWD2_80",&lj2_passWD2_80);
  Vtag->Branch("lj2_passWNtrk_50",&lj2_passWNtrk_50);
  Vtag->Branch("lj2_passWNtrk_80",&lj2_passWNtrk_80);
  Vtag->Branch("lj2_passWMassCut_50",&lj2_passWMassCut_50);
  Vtag->Branch("lj2_passWMassCut_80",&lj2_passWMassCut_80);
  Vtag->Branch("lj2_isWTagged_50",&lj2_isWTagged_50);
  Vtag->Branch("lj2_isWTagged_80",&lj2_isWTagged_80);

  Vtag->Branch("lj2_passZD2_50",&lj2_passZD2_50);
  Vtag->Branch("lj2_passZD2_80",&lj2_passZD2_80);
  Vtag->Branch("lj2_passZNtrk_50",&lj2_passZNtrk_50);
  Vtag->Branch("lj2_passZNtrk_80",&lj2_passZNtrk_80);
  Vtag->Branch("lj2_passZMassCut_50",&lj2_passZMassCut_50);
  Vtag->Branch("lj2_passZMassCut_80",&lj2_passZMassCut_80);
  Vtag->Branch("lj2_isZTagged_50",&lj2_isZTagged_50);
  Vtag->Branch("lj2_isZTagged_80",&lj2_isZTagged_80);

  Vtag->Branch("ptAsy",&ptAsy);
  Vtag->Branch("dy",&dy);

  Vtag->Branch("weight",&tot_weight);
  Vtag->Branch("MCWeight",&MCWeight);
  Vtag->Branch("SampleWeight",&SampleWeight);
  Vtag->Branch("PUWeight",&PUWeight);

  V50 = new TTree("V50","tree with V tag 50");
  V50->Branch("mass",&v_50_mass);
  V50->Branch("pt",&v_50_pt);
  V50->Branch("truthlabel",&v_50_truthlabel);
  V50->Branch("truthmass",&v_50_truthmass);
  V50->Branch("truthpt",&v_50_truthpt);
  V50->Branch("weight",&tot_weight);

  V80 = new TTree("V80","tree with V tag 80");
  V80->Branch("mass",&v_80_mass);
  V80->Branch("pt",&v_80_pt);
  V80->Branch("truthlabel",&v_80_truthlabel);
  V80->Branch("truthmass",&v_80_truthmass);
  V80->Branch("truthpt",&v_80_truthpt);
  V80->Branch("weight",&tot_weight);

}

void Nominal::SlaveBegin(TTree * /*tree*/)
{
   // The SlaveBegin() function is called after the Begin() function.
   // When running with PROOF SlaveBegin() is called on each slave server.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();
}

Bool_t Nominal::Process(Long64_t entry)
{

  //It is stupid to use RDF with entry, but it's the fastest way to minize the modification of the code.

   fReader.SetEntry(entry);
   float totalWeight = 1;
   tot_weight = totalWeight;
#ifndef processdata
   totalWeight = (*EventWeight)*mc_weighted*(*PileupWeight)/dsid_weights;
   SampleWeight = mc_weighted/dsid_weights;
   MCWeight = *EventWeight;
   PUWeight = *PileupWeight;
   tot_weight = totalWeight*lumi;
#endif

   std::vector<fatjet> largeRjets;
   std::vector<trackjet> trackjets;
   std::vector<muon> muons;

   int nlargeRjets = LargeRJetIsSelected.GetSize();
   int ntrackjets = TrackJetIsSelected.GetSize();
   int nmuons = MuonPt.GetSize();
   std::cout << "ntrackjets: " << ntrackjets << std::endl;

#ifndef processdata
   std::vector<fatjet> truthlargeRjets;
   int ntruthlargeRjets = TruthLargeRJetPt.GetSize();
   for (int i=0;i<ntruthlargeRjets; ++i) {
   fatjet temp;
   temp.SetPtEtaPhiM(TruthLargeRJetPt[i],TruthLargeRJetEta[i],TruthLargeRJetPhi[i],TruthLargeRJetMass[i]);
   temp.truthlabel = TruthLargeRJetTruthLabel[i];
   truthlargeRjets.push_back(temp);
   }
#endif

     for (int i=0;i<nlargeRjets; ++i) { 
       fatjet temp;
       temp.SetPtEtaPhiM(LargeRJetPt[i],LargeRJetEta[i],LargeRJetPhi[i],LargeRJetMass[i]);
#ifndef processdata
       temp.truthlabel = LargeRJetTruthLabel[i];
       temp.truthmass = LargeRJetTruthMass[i];
#endif
       temp.isPassWNtrk_50 = LargeRJetPassWNtrk_50[i];
       temp.isPassWD2_50 = LargeRJetPassWD2_50[i];
       temp.isPassWMassCut_50 = LargeRJetPassWMassHigh_50[i] && LargeRJetPassWMassLow_50[i];
       temp.isWTagged_50 = (LargeRJetPassWD2_50[i] && LargeRJetPassWNtrk_50[i]);
       temp.isPassWNtrk_80 = LargeRJetPassWNtrk_80[i];
       temp.isPassWD2_80 = LargeRJetPassWD2_80[i];
       temp.isPassWMassCut_80 = LargeRJetPassWMassHigh_80[i] &&LargeRJetPassWMassLow_80[i];
       temp.isWTagged_80 = (LargeRJetPassWD2_80[i] && LargeRJetPassWNtrk_80[i]);

       temp.isPassZNtrk_50 = LargeRJetPassZNtrk_50[i];
       temp.isPassZD2_50 = LargeRJetPassZD2_50[i];
       temp.isPassZMassCut_50 = LargeRJetPassZMassHigh_50[i] && LargeRJetPassZMassLow_50[i];
       temp.isZTagged_50 = (LargeRJetPassZD2_50[i] && LargeRJetPassZNtrk_50[i]);
       temp.isPassZNtrk_80 = LargeRJetPassZNtrk_80[i];
       temp.isPassZD2_80 = LargeRJetPassZD2_80[i];
       temp.isPassZMassCut_80 = LargeRJetPassZMassHigh_80[i] &&LargeRJetPassZMassLow_80[i];
       temp.isZTagged_80 = (LargeRJetPassZD2_80[i] && LargeRJetPassZNtrk_80[i]);

       temp.isMuonCorrection = 0;
       temp.Ntrk = LargeRJetNtrk[i];
       temp.ECF1 = LargeRJetECF1[i];
       temp.ECF2 = LargeRJetECF2[i];
       temp.ECF3 = LargeRJetECF3[i];
       temp.D2 = LargeRJetECF3[i]*pow(LargeRJetECF1[i],3)/pow(LargeRJetECF2[i],3);
       temp.Tau1_wta = LargeRJetTau1[i];
       temp.Tau2_wta = LargeRJetTau2[i];
       temp.Tau3_wta = LargeRJetTau3[i];
       temp.Tau21 = LargeRJetTau21[i];
       temp.Tau32 = LargeRJetTau3[i]/LargeRJetTau2[i];
       temp.FoxWolfram2 = LargeRJetFoxWolfram2[i];
       temp.FoxWolfram0 = LargeRJetFoxWolfram0[i];
       temp.Split12 = LargeRJetSplit12[i];
       temp.Split23 = LargeRJetSplit23[i];
       temp.Qw = LargeRJetQw[i];
       temp.PlanarFlow = LargeRJetPlanarFlow[i];
       temp.Angularity = LargeRJetAngularity[i];
       temp.Aplanarity = LargeRJetAplanarity[i];
       temp.ZCut12 = LargeRJetZCut12[i];
       temp.KtDR = LargeRJetKtDR[i];
       temp.HbbScoreTop = LargeRJetHbbScoreTop[i];
       temp.HbbScoreQCD = LargeRJetHbbScoreQCD[i];
       temp.HbbScoreHiggs = LargeRJetHbbScoreHiggs[i];
       largeRjets.push_back(temp);
     } // Close for (int i=0;i<nlargeRjets;++i)
  
   for (int i=0;i<ntrackjets;++i) {  
     trackjet temp; 
     temp.SetPtEtaPhiM(TrackJetPt[i],TrackJetEta[i],TrackJetPhi[i],TrackJetMass[i]);
#ifndef processdata
     temp.Truthlabel = TrackJetTruthLabelId[i];
#endif
     trackjets.push_back(temp);
   } // Close for (int i=0;i<ntrackjets;++i)
 
   for (int i=0;i<nmuons;++i) {
     muon temp;
     temp.SetPtEtaPhiM(MuonPt[i],MuonEta[i],MuonPhi[i],0.1056);
     temp.quality = MuonQuality[i];
     muons.push_back(temp);
   } // Close for (int i=0;i<nmuons;++i)

   FatToTrackLinks(largeRjets,trackjets,nlargeRjets);

   EraseUnselectedFatjets(largeRjets);
     
   fatjet * Tagged_50 = 0;
   fatjet * Tagged_80 = 0;
   fatjet * Zcand = 0;
   fatjet * leading_lrj = 0;
   fatjet * sub_leading_lrj = 0;

   double highestPt = 0;
   double secondPt = 0;
   double mass_low_cut = 0;

   ////////////////////////////////////////////////////////////////////////
    if(nlargeRjets > 1) {
     //only process events with at least 2 largeRjets;
      highestPt = 0;
      mass_low_cut = 30.;
      // find leading lrj 
      for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
	fatjet & largeR = (*lrj);
	if (largeR.isCand() && largeR.Pt() > highestPt && largeR.M() > mass_low_cut){
	  leading_lrj = &largeR;
	  highestPt = leading_lrj->Pt();
	}
	else continue;
      } 
 
      // find sub-leading lrj
      for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
	fatjet & largeR = (*lrj);
	if (largeR.isCand() && largeR.Pt() < highestPt && largeR.M() > mass_low_cut) {
	  if (largeR.Pt() > secondPt) {
	    sub_leading_lrj = &largeR;
	    secondPt = sub_leading_lrj->Pt();
	  }
	  else continue;
	}
	else continue;
      }
 
      if(leading_lrj != 0 && sub_leading_lrj != 0 ){
  

#ifndef processdata	// Find matched truth jet
	float dRtruth = 0.75;
	float dRmin = 0.75;
	for ( auto lrj = truthlargeRjets.begin();lrj != truthlargeRjets.end();++lrj ) {
	  fatjet & tlrj = (*lrj);
	  float dR = leading_lrj->DeltaR(tlrj);
	  if ( dR < dRtruth ) {
	    if( dR < dRmin ){
	      dRmin = dR;
	      lj1_truthmass = tlrj.M();
	      lj1_truthpt = tlrj.Pt();
	    }
	  }
	}
	dRmin = 0.75; //reset default dRmin
	for ( auto lrj = truthlargeRjets.begin();lrj != truthlargeRjets.end();++lrj ) {
	  fatjet & tlrj = (*lrj);
	  float dR = sub_leading_lrj->DeltaR(tlrj);
	  if ( dR < dRtruth ) {
	    if( dR < dRmin ){
	      dRmin = dR;
	      lj2_truthmass = tlrj.M();
	      lj2_truthpt = tlrj.Pt();
	    }
	  }
	}
#endif

	double symmetry = (leading_lrj->Pt() - sub_leading_lrj->Pt())/(leading_lrj->Pt() + sub_leading_lrj->Pt());
	double rapidity_cut = abs(leading_lrj->Rapidity() - sub_leading_lrj->Rapidity());
 
	NlargeRjets = nlargeRjets;
	ptAsy = symmetry;
	dy = rapidity_cut;
	lj1_mass = leading_lrj->M();
	lj1_mt = leading_lrj->Mt();
	lj1_pt = leading_lrj->Pt();
	lj1_eta = leading_lrj->Eta();
	lj1_phi = leading_lrj->Phi();
	lj1_D2 = leading_lrj->D2;
	lj1_Ntrk =leading_lrj->Ntrk;
	lj1_truthlabel = leading_lrj->truthlabel;

	lj1_passWD2_50 = leading_lrj->isPassWD2_50;
	lj1_passWD2_80 = leading_lrj->isPassWD2_80;
	lj1_passWNtrk_50 = leading_lrj->isPassWNtrk_50;
	lj1_passWNtrk_80 = leading_lrj->isPassWNtrk_80;
	lj1_passWMassCut_50 = leading_lrj->isPassWMassCut_50;
	lj1_passWMassCut_80 = leading_lrj->isPassWMassCut_80;
	lj1_isWTagged_50 = leading_lrj->isWTagged_50;
	lj1_isWTagged_80 = leading_lrj->isWTagged_80;

	lj1_passZD2_50 = leading_lrj->isPassZD2_50;
	lj1_passZD2_80 = leading_lrj->isPassZD2_80;
	lj1_passZNtrk_50 = leading_lrj->isPassZNtrk_50;
	lj1_passZNtrk_80 = leading_lrj->isPassZNtrk_80;
	lj1_passZMassCut_50 = leading_lrj->isPassZMassCut_50;
	lj1_passZMassCut_80 = leading_lrj->isPassZMassCut_80;
	lj1_isZTagged_50 = leading_lrj->isZTagged_50;
	lj1_isZTagged_80 = leading_lrj->isZTagged_80;

	lj2_mass = sub_leading_lrj->M();
	lj2_mt = sub_leading_lrj->Mt();
	lj2_pt = sub_leading_lrj->Pt();
	lj2_eta = sub_leading_lrj->Eta();
	lj2_phi = sub_leading_lrj->Phi();
	lj2_D2 =sub_leading_lrj->D2;
	lj2_Ntrk =sub_leading_lrj->Ntrk;
	lj2_truthlabel = sub_leading_lrj->truthlabel;

	lj2_passWD2_50 = sub_leading_lrj->isPassWD2_50;
	lj2_passWD2_80 = sub_leading_lrj->isPassWD2_80;
	lj2_passWNtrk_50 = sub_leading_lrj->isPassWNtrk_50;
	lj2_passWNtrk_80 = sub_leading_lrj->isPassWNtrk_80;
	lj2_passWMassCut_50 = sub_leading_lrj->isPassWMassCut_50;
	lj2_passWMassCut_80 = sub_leading_lrj->isPassWMassCut_80;
	lj2_isWTagged_50 = sub_leading_lrj->isWTagged_50;
	lj2_isWTagged_80 = sub_leading_lrj->isWTagged_80;

	lj2_passZD2_50 = sub_leading_lrj->isPassZD2_50;
	lj2_passZD2_80 = sub_leading_lrj->isPassZD2_80;
	lj2_passZNtrk_50 = sub_leading_lrj->isPassZNtrk_50;
	lj2_passZNtrk_80 = sub_leading_lrj->isPassZNtrk_80;
	lj2_passZMassCut_50 = sub_leading_lrj->isPassZMassCut_50;
	lj2_passZMassCut_80 = sub_leading_lrj->isPassZMassCut_80;
	lj2_isZTagged_50 = sub_leading_lrj->isZTagged_50;
	lj2_isZTagged_80 = sub_leading_lrj->isZTagged_80;

	Vtag->Fill();

	if( symmetry < 0.15){ //pT symmetry
	  if( rapidity_cut < 1.2){ //rapidity cut
	    //the tagged jet could be leading large-R jet of sub-leading large-R jet (eventually third-leading large-R jet)
	    // At first time, just try leading large-R jet of sub-leading large-R jet
	    // if both are tagged, just take leading large-R jet as tagged jet.
      
	    if( (leading_lrj->isWTagged_50 && !sub_leading_lrj->isPassWD2_50) || (leading_lrj->isZTagged_50 && !sub_leading_lrj->isPassZD2_50)) { 
	      Tagged_50 = leading_lrj;
	      v_50_truthmass = lj1_truthmass;
	      v_50_truthpt = lj1_truthpt;
	    } 
	      if(Tagged_50 == 0 && ((sub_leading_lrj->isWTagged_50 && !leading_lrj->isPassWD2_50) || (sub_leading_lrj->isZTagged_50 && !leading_lrj->isPassZD2_50))) {
	      Tagged_50 = sub_leading_lrj;
	      v_50_truthmass = lj2_truthmass;
	      v_50_truthpt = lj2_truthpt;
	    }
	      if( (leading_lrj->isWTagged_80 && !sub_leading_lrj->isPassWD2_80) || (leading_lrj->isZTagged_80 && !sub_leading_lrj->isPassZD2_80)) {
	      Tagged_80 = leading_lrj;
	      v_80_truthmass = lj1_truthmass;
	      v_80_truthpt = lj1_truthpt;
	    } 
	      if(Tagged_80 == 0 && ((sub_leading_lrj->isWTagged_80 && !leading_lrj->isPassWD2_80) || (sub_leading_lrj->isZTagged_80 && !leading_lrj->isPassZD2_80))) {
	      Tagged_80 = sub_leading_lrj;
	      v_80_truthmass = lj2_truthmass;
	      v_80_truthpt = lj2_truthpt;
	    }
         
	    if (Tagged_50 != 0) { 
	      v_50_mass = Tagged_50->M();
	      v_50_pt = Tagged_50->Pt();
	      v_50_truthlabel = Tagged_50->truthlabel;
	      V50->Fill();
	    } //if (Tagged_50 != 0)

	    if (Tagged_80 != 0) {
	      v_80_mass = Tagged_80->M();
	      v_80_pt = Tagged_80->Pt();
	      v_80_truthlabel = Tagged_80->truthlabel;
	      V80->Fill();
	    }//(Tagged_80 != 0)


      //-------------------------------------------------------------------------------------
      //Zbb
      //-----------------------------------------------

	    if(leading_lrj->isCand()){
	      if(leading_lrj->M() > 50){
		if(leading_lrj->Pt() > 450){
		  Zcand = leading_lrj;
		}
	      } //Get a Z candidate
	    }
   
	    trackjet * Zcand_leadingtrk = 0;
	    trackjet * Zcand_secondtrk = 0;
	    trackjet * Zcand_thirdtrk = 0;
	    double Zcand_highestPt = 0;
	    double Zcand_secondPt = 0;
	    double Zcand_thirdPt  = 0;

	    trackjet * sub_leadingtrk = 0;
	    trackjet * sub_secondtrk = 0;
	    trackjet * sub_thirdtrk = 0;
	    double sub_highestPt = 0;
	    double sub_secondPt = 0;
	    double sub_thirdPt  = 0;

	    if (Zcand != 0) { // Get Zcand_leadingtrk Zcand_secondtrk        
	      if(Zcand->GetNumTrackVectors()>0){
		for (int i=0;i<Zcand->GetNumTrackVectors();++i) {
		  trackjet * Zcand_trk = Zcand->GetTrackVector(i);
		  if (Zcand_trk->Pt() > Zcand_highestPt) {
		    Zcand_thirdPt = Zcand_secondPt;
		    Zcand_thirdtrk = Zcand_secondtrk;
		    Zcand_secondPt = Zcand_highestPt;
		    Zcand_secondtrk = Zcand_leadingtrk;
		    Zcand_leadingtrk = Zcand_trk;
		    Zcand_highestPt = Zcand_trk->Pt();
		  } else if (Zcand_trk->Pt() > Zcand_secondPt) {
		    Zcand_thirdPt = Zcand_secondPt;
		    Zcand_thirdtrk = Zcand_secondtrk;
		    Zcand_secondtrk = Zcand_trk;
		    Zcand_secondPt = Zcand_trk->Pt();
		  } else if (Zcand_trk->Pt() > Zcand_thirdPt) {
		    Zcand_thirdtrk = Zcand_trk;
		    Zcand_thirdPt = Zcand_thirdtrk->Pt();
		  } else continue;
		}
	      }
	      if(sub_leading_lrj->GetNumTrackVectors()>0){
		for (int i=0;i<sub_leading_lrj->GetNumTrackVectors();++i) {
		  trackjet * sub_trk = sub_leading_lrj->GetTrackVector(i);
		  if (sub_trk->Pt() > sub_highestPt) {
		    sub_thirdPt = sub_secondPt;
		    sub_thirdtrk = sub_secondtrk;
		    sub_secondPt = sub_highestPt;
		    sub_secondtrk = sub_leadingtrk;
		    sub_leadingtrk = sub_trk;
		    sub_highestPt = sub_trk->Pt();
		  } else if (sub_trk->Pt() > sub_secondPt) {
		    sub_thirdPt = sub_secondPt;
		    sub_thirdtrk = sub_secondtrk;
		    sub_secondtrk = sub_trk;
		    sub_secondPt = sub_trk->Pt();
		  } else if (sub_trk->Pt() > sub_thirdPt) {
		    sub_thirdtrk = sub_trk;
		    sub_thirdPt = sub_trk->Pt();;
		  } else continue;
		}
	      }
   
	      if (Zcand != 0 ) { // Definitely got a Zcand and it has highest pT

		lj1_mass = Zcand->M();
		lj1_pt = Zcand->Pt();
		lj1_eta = Zcand->Eta();
		lj1_phi = Zcand->Phi();
		lj1_truthlabel = Zcand->truthlabel;
		lj1_nVRjets = Zcand->GetNumTrackVectors();
		lj1_VRj1_id = -1;
		lj1_VRj2_id = -1;
		lj1_VRj3_id = -1;
		lj2_VRj1_id = -1;
		lj2_VRj2_id = -1;
		lj2_VRj3_id = -1;
		if(Zcand_leadingtrk !=0 ) lj1_VRj1_id = Zcand_leadingtrk->Truthlabel; else lj1_VRj1_id = -1;
		if(Zcand_secondtrk !=0 ) lj1_VRj2_id = Zcand_secondtrk->Truthlabel; else lj1_VRj2_id = -1;
		if(Zcand_thirdtrk !=0 ) lj1_VRj3_id = Zcand_thirdtrk->Truthlabel; else lj1_VRj3_id = -1;
		lj1_XbbDiscriminant_f25 = Zcand->GetXbbDiscriminant();
		if(Zcand->GetXbb_50()) lj1_isXbb_50 = 1; else lj1_isXbb_50 = 0;
		if(Zcand->GetXbb_60()) lj1_isXbb_60 = 1; else lj1_isXbb_60 = 0;
		if(Zcand->GetXbb_70()) lj1_isXbb_70 = 1; else lj1_isXbb_70 = 0;
		if(Zcand->GetXbb_80()) lj1_isXbb_80 = 1; else lj1_isXbb_80 = 0;
		lj1_D2 =  Zcand->D2;
		lj1_Ntrk =  Zcand->Ntrk;


		lj2_mass = sub_leading_lrj->M();
		lj2_pt = sub_leading_lrj->Pt();
		lj2_eta = sub_leading_lrj->Eta();
		lj2_phi = sub_leading_lrj->Phi();
		lj2_truthlabel = sub_leading_lrj->truthlabel;
		lj2_nVRjets = sub_leading_lrj->GetNumTrackVectors();
		if(sub_leadingtrk !=0 ) lj2_VRj1_id = sub_leadingtrk->Truthlabel; else lj2_VRj1_id = -1;
		if(sub_secondtrk !=0 ) lj2_VRj2_id = sub_secondtrk->Truthlabel; else lj2_VRj2_id = -1;
		if(sub_thirdtrk !=0 ) lj2_VRj3_id = sub_thirdtrk->Truthlabel; else lj2_VRj3_id = -1;
		lj2_XbbDiscriminant_f25 = sub_leading_lrj->GetXbbDiscriminant();
		if(sub_leading_lrj->GetXbb_50()) lj2_isXbb_50 = 1; else lj2_isXbb_50 = 0;
		if(sub_leading_lrj->GetXbb_60()) lj2_isXbb_60 = 1; else lj2_isXbb_60 = 0;
		if(sub_leading_lrj->GetXbb_70()) lj2_isXbb_70 = 1; else lj2_isXbb_70 = 0;
		if(sub_leading_lrj->GetXbb_80()) lj2_isXbb_80 = 1; else lj2_isXbb_80 = 0;
		lj2_D2 =  sub_leading_lrj->D2;
		lj2_Ntrk = sub_leading_lrj->Ntrk;

		Btag->Fill();
	      }
	    }// Close if (Zcand != 0)
	    ////////////////////////////////////////////////////
	    //end Vbb
   
	  } // Close if (Pt symmetry)
	}// Close if (Eta cut)
	// std::cout<<"End of Process"<<std::endl;
      }//Close if leading !=0 && sub_leading !=0 
      ///////////////////////////////////////////////////////////////////////////////
    } //Close if (nlargeRjet > 1)
    /////////////////////////////////////////////////////////////////////////////
    return kTRUE;
} // Close Nominal

void Nominal::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}

void Nominal::Terminate(TH1D* hcutflow,TString mc_filename)
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.

  
  TFile *f = new TFile(mc_filename+".root","RECREATE");
  hcutflow->Write();
  Btag->Write();
  Vtag->Write();
  V50->Write();
  V80->Write();

  f->Close();
  Btag->Delete();
  Vtag->Delete();
  V50->Delete();
  V80->Delete();
}

TH1D * Nominal::makeHist(TString name,int nbins,double x1, double x2) {
  if(name.Contains("mass")) nbins = nbins*1;
  TH1D * hist = new TH1D(name,name,nbins,x1,x2);
  hist->Sumw2();
  histlist.push_back(hist);
  return hist;
}

TH2D * Nominal::makeHist2(TString name,int xnbins,double x1, double x2, int ynbins, double y1, double y2) {
  TH2D * hist2 = new TH2D(name,name,xnbins,x1,x2,ynbins,y1,y2);
  hist2->Sumw2();
  histlist2.push_back(hist2);
  return hist2;
}

void Nominal::FatToTrackLinks(std::vector<fatjet> & largeRjets,std::vector<trackjet> & trackjets,int & nlargeRjets){
  //  std::cout<<"Creating links between fatjet and trackjets."<<std::endl;
  for (int i=0;i<nlargeRjets;++i) {
    //    std::cout<<"i= "<<i<<std::endl;
    fatjet & fj = largeRjets[i];
    //    std::cout<<"We have largeRjet "<<i<<std::endl;
    std::vector<unsigned long> tjvector = LargeRJetAssociatedTrackJet[i];
    //    std::cout<<"We have the vector of trackjets."<<std::endl;
    int ntj = 0;
    for (auto tj=tjvector.begin();tj!=tjvector.end();++tj) { 
      trackjet & tjassociated = trackjets[*tj];
      std::cout << "which trk: " << *tj << std::endl; 
      //if (TrackJetIsSelected[*tj] == false) continue;
      //else if (TrackJetPassDR[*tj] == false) continue;
      if (TrackJetPassDR[*tj] == false) continue;
      ntj = ntj+1;
      (fj).AddTrackJetPointer(&(tjassociated));
    }
    //std::cout<< "number of associated tj : "<< ntj << std::endl;
  }
  //  std::cout<<"Exiting for loop."<<std::endl;
  }

void Nominal::EraseUnselectedFatjets(std::vector<fatjet> & largeRjets) {
  for (uint i=0;i<largeRjets.size();++i) {
    if (LargeRJetIsSelected[i] == false) {
      largeRjets.erase(largeRjets.begin()+i);
    }
    else continue;
  }
}

void Nominal::MuonInJetCorrection(fatjet* largeRjet, trackjet* trackjet, std::vector<muon> & muons){
  //it is not mature, more work and check needed here
  double trkj_mu_deltaR0 = 0.4;
  muon *muon_in_jet = 0;
  fatjet & largeR = (*largeRjet);
  for (auto mu = muons.begin();mu != muons.end();++mu ){
    muon & muon_cand = (*mu);
    double trkj_mu_deltaR = trackjet->DeltaR(muon_cand);
    int num_muon_in_jet = 0;
    if(muon_cand.quality == 0 || muon_cand.quality == 1){
    //std::cout << "trkj_mu_deltaR : " << trkj_mu_deltaR << std::endl;
    // if(trkj_mu_deltaR < 0.4 && trkj_mu_deltaR < 0.04 + 10.0/muon_in_jet.Pt()){
    if(trkj_mu_deltaR < 0.4){
      if(num_muon_in_jet == 0){
      num_muon_in_jet = num_muon_in_jet + 1;
      trkj_mu_deltaR0 =trkj_mu_deltaR;
      muon_in_jet = &muon_cand;
      }
      else if(trkj_mu_deltaR < trkj_mu_deltaR0){
	num_muon_in_jet =num_muon_in_jet+ 1;
	trkj_mu_deltaR0 = trkj_mu_deltaR;
	muon_in_jet = &muon_cand;
      }
    }
  }
  }

  if(muon_in_jet != 0){
    //std::cout << "We get a muon in jet: " <<  trkj_mu_deltaR0 <<std::endl;
      largeR.isMuonCorrection = 1;
      //std::cout << "We get a muon in trk jet: " <<  num_trkj_correction + 1 <<std::endl;
      //std::cout << "We get a muon in lrj jet: " <<  num_lrj_correction + 1 <<std::endl;
      //std::cout << "before correction " <<  trkj->M() << "  " << largeRjet->M() <<std::endl;
      trackjet->SetPtEtaPhiM(trackjet->Pt()+muon_in_jet->Pt(),trackjet->Eta(),trackjet->Phi(),trackjet->M()+muon_in_jet->M());
      largeRjet->SetPtEtaPhiM(largeRjet->Pt()+muon_in_jet->Pt(),largeRjet->Eta(),largeRjet->Phi(),largeRjet->M()+muon_in_jet->M());
      //std::cout << "after correction " <<  trkj->M() << "  "<< largeRjet->M() << " " << largeRjet->isMuonCorrection<< std::endl;
  }

}
    
std::pair<std::map<std::string, double>, bool> GetMCWeightMap() {
  std::map<std::string, double> MCWeightMap;
  bool Success;
  PyObject *pName;
  PyObject *pModule;
  PyObject *pFunc;
  PyObject *pDict;

  Py_Initialize();
  PyRun_SimpleString("import sys");
  PyRun_SimpleString("sys.path.append(\".\")");
  pName = PyString_FromString("read_json");
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);

  if (pModule != nullptr) {
    pFunc = PyObject_GetAttrString(pModule,"read_file");
    if (pFunc && PyCallable_Check(pFunc)) {
      pDict = PyObject_CallObject(pFunc, nullptr);
      if (pDict != nullptr) {
	PyObject *pKeys = PyDict_Keys(pDict);
	PyObject *pValues = PyDict_Values(pDict);
	for (Py_ssize_t i = 0; i < PyDict_Size(pDict); ++i) { 
	  auto pair = std::make_pair(std::string(PyString_AsString(PyList_GetItem(pKeys, i))), PyFloat_AsDouble(PyList_GetItem(pValues, i)));
	  MCWeightMap.insert( pair );
	}
	Success = true;
	Py_DECREF(pDict);
      }
      else {
	Py_DECREF(pFunc);
	Py_DECREF(pModule);
	PyErr_Print();
	fprintf(stderr,"Call failed\n");
	Success = false;
	return std::make_pair(MCWeightMap,Success);
      }
    }
    else {
      if (PyErr_Occurred())
	PyErr_Print();
      fprintf(stderr,"Cannot find function \"%s\"\n", "read_file");
    }
    Py_XDECREF(pFunc);
    Py_DECREF(pModule);
  }
  else {
    PyErr_Print();
    fprintf(stderr, "Failed to load \"%s\"\n", "read_json");
    Success = false;
    return std::make_pair(MCWeightMap,Success);
  }
  Py_Finalize();
  Success = true;
  //for (auto const& pair: MCWeightMap) {
  //   std::cout<<"{" <<(pair.first) << ":"<<(pair.second)<<"}"<<std::endl;
  // }

  return std::make_pair(MCWeightMap,Success);
}	    

std::vector<TString> list_input_files(const char *inputfile = "in.txt"){
  std::vector<TString> result;
  std::ifstream input_list_file(inputfile);
  std::string line;
  if (input_list_file.is_open())
    {
      while ( getline (input_list_file,line) )
        {
          result.push_back(line);
        }
      input_list_file.close();
    }

  else std::cout << "Unable to open file" << std::endl;
  return result;
  }


std::vector<TString> list_files(const char *dirname="C:/root/folder/", const char *ext=".root") {
  std::vector<TString> result;
  TSystemDirectory dir(dirname, dirname);
  TList *files = dir.GetListOfFiles();
  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file=(TSystemFile*)next())) {
      fname = file->GetName();
      if (!file->IsDirectory() && fname.EndsWith(ext)) {
     	std::cout << "list_files: " <<fname.Data() << std::endl;
	result.push_back(dirname+fname);
      }
      if(file->IsDirectory() && fname.EndsWith("_root")){
	TString subdirname = dirname + fname + "/";
	TSystemDirectory subdir(subdirname,subdirname);
	TList *subfiles = subdir.GetListOfFiles();
	if(subfiles){
	  TSystemFile *subfile;
	  TString subfname;
	  TIter subnext(subfiles);
	  while((subfile=(TSystemFile*)subnext())) {
	   subfname = subfile->GetName();
	    if (!subfile->IsDirectory() && subfname.EndsWith(ext)) {
	      std::cout << "list_files: " <<subfname.Data() << std::endl;
	      result.push_back(dirname+fname+"/"+subfname);
	    }//if (!subfile->IsDirectory() && subfname.EndsWith(ext))
	  } //while((subfile=(TSystemFile*)subnext()))
	}//(subfiles)
      }//(file->IsDirectory() && fname.EndsWith("_root"))
    }//while ((file=(TSystemFile*)next()))
  }//if (files)
  return result;
}

TH1D* cutflow(TString cutflowname, TString inputfile){
  std::vector<TString> list = list_input_files(inputfile);
  //std::vector<TString> list = list_files(inputfile);
  std::cout <<list[0] << std::endl;
  TFile * inputfile0 = TFile::Open(list[0]);
  inputfile0->cd("Analysis");
  TH1D * hcutflow = (TH1D*)gROOT->FindObject(cutflowname);
  hcutflow->SetDirectory(0);
  inputfile0->Close();
  for (uint i=1;i<list.size();++i) {
    std::cout <<list[i] << std::endl;
    TFile * inputfile = TFile::Open(list[i]);
    inputfile->cd("Analysis");
    TH1D * temp = (TH1D*)gROOT->FindObject(cutflowname);
    if(hcutflow == 0) {
      hcutflow = temp;
      if (hcutflow !=0 ) hcutflow->SetDirectory(0);
    } else {
      hcutflow->Add(temp);
    }
    inputfile->Close();
  }
  return hcutflow;
}

TChain* CreateSingleChain(TString whichsystematic, TString inputfile){
  std::vector<TString> list = list_input_files(inputfile);
  //std::vector<TString> list = list_files(inputfile);
  TChain* syst_chain = new TChain(whichsystematic);
  std::cout<<"AddFilesToChain: Found "<<list.size()<<" files"<<std::endl;
  for (uint i=0;i<list.size();++i) {
    std::cout<<"AddFilesToChain: Adding"<<list[i]<< "to chain: " << whichsystematic << std::endl;
    syst_chain->Add(list[i]);
  }
  std::cout<<"CreateSingleChain: Will use "<<syst_chain->GetEntries()<<" entries"<<std::endl;
  syst_chain->Lookup(true);
  return syst_chain;
}

TChain* CreateSystChain(TString whichsystematic, TString inputfile){
  TChain* nominal_chain = new TChain ("nominal");
  TChain* syst_chain = new TChain (whichsystematic);
  std::cout << "Do systenmatics !!!!!" << std::endl;
  std::cout << "Systematic: " << whichsystematic << std::endl;
  //syst_chain->AddFriend(nominal_chain);
  std::vector<TString> list = list_input_files(inputfile);
  //std::vector<TString> list = list_files(inputfile);
  std::cout<<"AddFilesToChain: Found "<<list.size()<<" files"<<std::endl;  
  for (uint i=0;i<list.size();++i) {
    std::cout<<"AddFilesToChain: Adding"<<list[i]<< "to chain: " << whichsystematic << std::endl;
    nominal_chain->Add(list[i]);
    syst_chain->Add(list[i]);
  }
  std::cout<<"CreateSystChain: Will use "<<syst_chain->GetEntries()<<" entries"<<std::endl;
  std::cout<<"CreateNominalChain: Will use "<<nominal_chain->GetEntries()<<" entries"<<std::endl;
  nominal_chain->Lookup(true);
  syst_chain->Lookup(true);
  syst_chain->AddFriend(nominal_chain);
 return syst_chain;
}

void dogen(TString input,TString fname, TString fwhichsystematics, TString pTbin, double highpT, double lowpT) {
  Nominal * selector = new Nominal();
#ifdef processdata
  TH1D* ntupleCutFlow = cutflow("cutflow", input);
#endif
#ifndef processdata
  TH1D* ntupleCutFlow = cutflow("cutflow_mc_pu", input);
#endif
  TChain * chain_syst = new TChain(fwhichsystematics);
  TChain * chain_nominal = new TChain("nominal");
  TChain * chain_2 = new TChain("sumWeights");
  /*if(fwhichsystematics != "nominal") {
    chain_syst = CreateSystChain(fwhichsystematics,input);
    // chain_nominal = CreateSingleChain("nominal",input);
  } else {
    chain_syst = CreateSingleChain("nominal",input);
    }*/  
  //TChain * chain_2 = CreateSingleChain("sumWeights",input);
  TFileCollection fc("dum","",input);
    //TList *filelist = ;
  if(fwhichsystematics != "nominal") {
    chain_syst->AddFileInfoList(fc.GetList());
    chain_nominal->AddFileInfoList(fc.GetList());
    chain_syst->AddFriend(chain_nominal);
  } else {
    chain_syst->AddFileInfoList(fc.GetList());
  }
  chain_2->AddFileInfoList(fc.GetList());

  TString mc_filename = input;
  TString Sub_compaign = "r9364_";
  TString mc_filename_b = mc_filename;
  int k=0;
  while (mc_filename_b.Contains("/")){
    ++k;
    TString sub=mc_filename_b(0,mc_filename_b.First('/'));
    mc_filename_b.Replace(0,mc_filename_b.First('/')+1,"");
    std::cout << mc_filename_b << std::endl;
  }
  mc_filename_b.ReplaceAll('-',' ');
  TString mc_compaign = mc_filename_b(0,mc_filename_b.First(' '));
  TString mc_dsid = mc_filename_b(mc_filename_b.First(' ')+1,mc_filename_b.First(' ')+2);
  mc_dsid.ReplaceAll(' ',"");
  std::cout << mc_compaign << std::endl;
  std::cout << mc_dsid << "check"<< std::endl;

  float luminosity = 1.0;
  if(mc_filename.Contains("mc16a")) luminosity = 36.1e+06;
  if(mc_filename.Contains("mc16d")) luminosity = 44.3e+06;
  if(mc_filename.Contains("mc16e")) luminosity = 58.45e+06;
  if(mc_filename.Contains("mc16a")) Sub_compaign = "r9364_";
  if(mc_filename.Contains("mc16d")) Sub_compaign = "r10201_";
  if(mc_filename.Contains("mc16e")) Sub_compaign = "r10724_";
  

  std::map<std::string, double> weight_map = (GetMCWeightMap()).first;
  for (auto const& pair: weight_map) {
    std::cout<<"{" <<(pair.first) << ":"<<(pair.second)<<"}"<<std::endl;
   }
   for (auto iter = weight_map.begin(); iter != weight_map.end(); ++iter) {
     if(iter->first.find(Sub_compaign) !=std::string::npos && iter->first.find(mc_dsid) !=std::string::npos ){
      selector->mc_weighted = iter->second;
      std::cout<< iter->first << " : "<< "mc_weighted"<< " : " << selector->mc_weighted<<std::endl;
    }
   }
   //std::cout<<mc_filename<< " : "<< "mc_weighted"<< " : " << selector->mc_weighted<<std::endl;
  //std::cout<<"dogen: Will use "<<chain_syst->GetEntries()<<" entries"<<std::endl;
  TTreeReader sumWeightsReader;
  sumWeightsReader.SetTree(chain_2);
  TTreeReaderValue<Float_t> totalEventsWeighted = {sumWeightsReader,"totalEventsWeighted"};
  
  selector->Init(chain_syst);
  selector->Begin(0);
  selector->Notify();
  //selector->fChain = (TTree*)chain_syst->Clone();
  std::cout<<"dogen: Will use "<<chain_syst->GetEntries()<<" entries"<<std::endl;

  std::cout << "sumweights entries : " << chain_2->GetEntries() << std::endl; 
  //while(sumWeightsReader.Next()){
  //  selector->dsid_weights+=*totalEventsWeighted;
  // }
  std::cout << "sumweights entries is OK" << std::endl;

  for (int i=0; i<chain_2->GetEntries();++i) {
    //std::cout<< i << std::endl;
    sumWeightsReader.SetEntry(i);    
    //std::cout<< "totalEventsWeighted : " << *totalEventsWeighted<<std::endl;
    selector->dsid_weights+=*totalEventsWeighted;
    std::cout<<"dsid_weights : " << selector->dsid_weights<<std::endl;
   }
   selector->do_which_systematic = fwhichsystematics;
   selector->lumi = luminosity;
   #ifndef processdata
   ntupleCutFlow->Scale(selector->mc_weighted*luminosity/selector->dsid_weights);
   #endif
   //std::cout<< chain_syst->GetEntry(5) << std::endl;
   for (int i=0;i<chain_syst->GetEntries();++i) {
     //std::cout<< i << std::endl;
     selector->Process(i);
   }
   std::cout<<"after selection: " << selector->Btag->GetEntries() << std::endl;
   std::cout<<"Process finished"<<std::endl;
   selector->Terminate(ntupleCutFlow,mc_filename+fwhichsystematics+pTbin);

       //std::cout<<"Will execute Exec(mv mc_filename.root "<< fname+ "_"+fwhichsystematics << ".root"")";
       //gSystem->Exec("mv "+mc_filename+fwhichsystematics+".root "+fname+"_"+fwhichsystematics+".root");
   std::cout<<"Will execute Exec(mv mc_filename.root "<< fname<<"-"<<fwhichsystematics<<".root)";
   gSystem->Exec("mv "+mc_filename+fwhichsystematics+pTbin+".root "+ fname+"-"+pTbin+"-"+fwhichsystematics+".root");
   selector->Delete();
}


int main(int argc, char **argv) {
  if (argc < 3) {
    std::cout<<"main called with only "<<argc<<" arguments; quit"<<std::endl;
    return 1;
  }
  double highpT= 0.;
  double lowpT = 5000.;
  TString infile = argv[1];
  TString outfile = argv[2];
  TString whichsystematic = argv[3];
  TString pTbin = "pTfull";

  std::cout<<"Nominal: infile "<<infile<<std::endl;
  std::cout<<"Nominal: outfile "<<outfile<<std::endl;
  std::cout<<"Systematic: " << whichsystematic <<std::endl;
  std::cout<<"pT bin high: " << highpT <<std::endl;
  std::cout<<"pT bin low: " << lowpT <<std::endl;
  dogen(infile,outfile,whichsystematic,pTbin,highpT,lowpT);
  return 0;
}

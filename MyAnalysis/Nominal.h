//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Apr  3 17:39:23 2019 by ROOT version 6.14/08
// from TTree nominal/tree
//////////////////////////////////////////////////////////

#ifndef Nominal_h
#define Nominal_h
#include "processtype.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TSelector.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include "fatjet.h"

#ifndef processdata
#include "truthfatjet.h"
#endif

// Headers needed by this particular selector
#include <vector>
#include <string>
#include <map>


class Nominal : public TSelector {
public :
   TTreeReader    fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain
   std::vector<TString> systs_list;
   TString do_which_systematic;
   // Readers to access the data (delete the ones you do not need).
#ifndef processdata
   TTreeReaderValue<uint32_t> DSID = {fReader, "DSID"}; // for mc
   TTreeReaderValue<unsigned long long> EventNumber = {fReader, "EventNumber"};
   TTreeReaderValue<Float_t> EventWeight = {fReader, "EventWeight"}; // for mc
   TTreeReaderValue<Float_t> PileupWeight = {fReader, "PileupWeight"}; //for mc
   TTreeReaderArray<float> TrackJetTruthLabelId = {fReader, "TrackJetHadronConeExclTruthLabelID"};
   TTreeReaderArray<int> TruthLargeRJetTruthLabel = {fReader, "TruthLargeRJetTruthLabel"};
   TTreeReaderArray<float> TruthLargeRJetPt = {fReader, "TruthLargeRJetPt"};
   TTreeReaderArray<float> TruthLargeRJetEta = {fReader, "TruthLargeRJetEta"};
   TTreeReaderArray<float> TruthLargeRJetPhi = {fReader, "TruthLargeRJetPhi"};
   TTreeReaderArray<float> TruthLargeRJetMass = {fReader, "TruthLargeRJetMass"};
   TTreeReaderArray<float> LargeRJetTruthMass = {fReader, "LargeRJetTruthMass"};
   TTreeReaderArray<int> LargeRJetTruthLabel = {fReader, "LargeRJetTruthLabel"};
#endif

   TTreeReaderArray<char> TrackJetIsSelected = {fReader, "TrackJetIsSelected"};
   TTreeReaderArray<float> TrackJetPt = {fReader, "TrackJetPt"};
   TTreeReaderArray<float> TrackJetEta = {fReader, "TrackJetEta"};
   TTreeReaderArray<float> TrackJetPhi = {fReader, "TrackJetPhi"};
   TTreeReaderArray<float> TrackJetMass = {fReader, "TrackJetMass"};
   TTreeReaderArray<char> TrackJetPassDR = {fReader, "TrackJetPassDR"};

   TTreeReaderArray<char> ElectronIsSelected = {fReader, "ElectronIsSelected"};
   TTreeReaderArray<char> ElectronIsSelectedLoose = {fReader, "ElectronIsSelectedLoose"};
   TTreeReaderArray<float> ElectronPt = {fReader, "ElectronPt"};
   TTreeReaderArray<float> ElectronEta = {fReader, "ElectronEta"};
   TTreeReaderArray<float> ElectronPhi = {fReader, "ElectronPhi"};
   TTreeReaderArray<char> MuonIsSelected = {fReader, "MuonIsSelected"};
   TTreeReaderArray<char> MuonIsSelectedLoose = {fReader, "MuonIsSelectedLoose"};

   TTreeReaderArray<float> MuonPt = {fReader, "MuonPt"};
   TTreeReaderArray<float> MuonEta = {fReader, "MuonEta"};
   TTreeReaderArray<float> MuonPhi = {fReader, "MuonPhi"};
   TTreeReaderArray<unsigned char> MuonQuality = {fReader, "MuonQuality"};

   TTreeReaderArray<char> LargeRJetIsSelected = {fReader, "LargeRJetIsSelected"};
   TTreeReaderArray<float> LargeRJetPt = {fReader, "LargeRJetPt"};
   TTreeReaderArray<float> LargeRJetEta = {fReader, "LargeRJetEta"};
   TTreeReaderArray<float> LargeRJetPhi = {fReader, "LargeRJetPhi"};
   TTreeReaderArray<float> LargeRJetNtrk = {fReader, "LargeRJetNtrk"};
   TTreeReaderArray<float> LargeRJetMass = {fReader, "LargeRJetMass"};
   TTreeReaderArray<float> LargeRJetECF1 = {fReader, "LargeRJetECF1"};
   TTreeReaderArray<float> LargeRJetECF2 = {fReader, "LargeRJetECF2"};
   TTreeReaderArray<float> LargeRJetECF3 = {fReader, "LargeRJetECF3"};
   TTreeReaderArray<float> LargeRJetD2 = {fReader, "LargeRJetD2"};
   TTreeReaderArray<float> LargeRJetTau1 = {fReader, "LargeRJetTau1_wta"};
   TTreeReaderArray<float> LargeRJetTau2 = {fReader, "LargeRJetTau2_wta"};
   TTreeReaderArray<float> LargeRJetTau3 = {fReader, "LargeRJetTau3_wta"};
   TTreeReaderArray<float> LargeRJetTau21 = {fReader, "LargeRJetTau21"};
   TTreeReaderArray<float> LargeRJetFoxWolfram2 = {fReader, "LargeRJetFoxWolfram2"};
   TTreeReaderArray<float> LargeRJetFoxWolfram0 = {fReader, "LargeRJetFoxWolfram0"};
   TTreeReaderArray<float> LargeRJetSplit12 = {fReader, "LargeRJetSplit12"};
   TTreeReaderArray<float> LargeRJetSplit23 = {fReader, "LargeRJetSplit23"};
   TTreeReaderArray<float> LargeRJetQw = {fReader, "LargeRJetQw"};
   TTreeReaderArray<float> LargeRJetPlanarFlow = {fReader, "LargeRJetPlanarFlow"};
   TTreeReaderArray<float> LargeRJetAngularity = {fReader, "LargeRJetAngularity"};
   TTreeReaderArray<float> LargeRJetAplanarity = {fReader, "LargeRJetAplanarity"};
   TTreeReaderArray<float> LargeRJetZCut12 = {fReader, "LargeRJetZCut12"};
   TTreeReaderArray<float> LargeRJetKtDR = {fReader, "LargeRJetKtDR"};

   TTreeReaderArray<float> LargeRJetXbbScoreTop = {fReader, "LargeRJetXbbScoreTop"};
   TTreeReaderArray<float> LargeRJetXbbScoreQCD = {fReader, "LargeRJetXbbScoreQCD"};
   TTreeReaderArray<float> LargeRJetXbbScoreHiggs = {fReader, "LargeRJetXbbScoreHiggs"};
   TTreeReaderArray<float> LargeRJetHbbScoreTop = {fReader, "LargeRJetHbbScoreTop"};
   TTreeReaderArray<float> LargeRJetHbbScoreQCD = {fReader, "LargeRJetHbbScoreQCD"};
   TTreeReaderArray<float> LargeRJetHbbScoreHiggs = {fReader, "LargeRJetHbbScoreHiggs"};
   
   TTreeReaderArray<int> LargeRJetPassWD2_80 = {fReader, "LargeRJetPassWD2_80"};
   TTreeReaderArray<int> LargeRJetPassWNtrk_80 = {fReader, "LargeRJetPassWNtrk_80"};
   TTreeReaderArray<int> LargeRJetPassWMassHigh_80 = {fReader, "LargeRJetPassWMassHigh_80"};
   TTreeReaderArray<int> LargeRJetPassWMassLow_80 = {fReader, "LargeRJetPassWMassLow_80"};
   TTreeReaderArray<int> LargeRJetPassZD2_80 = {fReader, "LargeRJetPassZD2_80"};
   TTreeReaderArray<int> LargeRJetPassZNtrk_80 = {fReader, "LargeRJetPassZNtrk_80"};
   TTreeReaderArray<int> LargeRJetPassZMassHigh_80 = {fReader, "LargeRJetPassZMassHigh_80"};
   TTreeReaderArray<int> LargeRJetPassZMassLow_80 = {fReader, "LargeRJetPassZMassLow_80"};
   
   TTreeReaderArray<int> LargeRJetPassWD2_50 = {fReader, "LargeRJetPassWD2_50"};
   TTreeReaderArray<int> LargeRJetPassWNtrk_50 = {fReader, "LargeRJetPassWNtrk_50"};
   TTreeReaderArray<int> LargeRJetPassWMassHigh_50 = {fReader, "LargeRJetPassWMassHigh_50"};
   TTreeReaderArray<int> LargeRJetPassWMassLow_50 = {fReader, "LargeRJetPassWMassLow_50"};
   TTreeReaderArray<int> LargeRJetPassZD2_50 = {fReader, "LargeRJetPassZD2_50"};
   TTreeReaderArray<int> LargeRJetPassZNtrk_50 = {fReader, "LargeRJetPassZNtrk_50"};
   TTreeReaderArray<int> LargeRJetPassZMassHigh_50 = {fReader, "LargeRJetPassZMassHigh_50"};
   TTreeReaderArray<int> LargeRJetPassZMassLow_50 = {fReader, "LargeRJetPassZMassLow_50"};

   TTreeReaderArray<std::vector<unsigned long>> LargeRJetAssociatedTrackJet = {fReader, "LargeRJetAssociatedTrackJet"};
   TTreeReaderValue<Bool_t> Analysis = {fReader, "Analysis"};
  
   //TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_60"};
   //TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_70"};
   //TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_77"};
   //TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_85"};

   TTree *Btag;
   TTree *Vtag;
   float lj1_mass;
   float lj1_mt;
   float lj1_pt;
   int lj1_truthlabel;
   float lj1_truthmass;
   float lj1_eta;
   float lj1_phi;
   float lj1_truthpt;
   int lj1_nVRjets;
   int lj1_VRj1_id;
   int lj1_VRj2_id;
   int lj1_VRj3_id;
   float lj1_XbbDiscriminant_f25;
   int lj1_isXbb_50;
   int lj1_isXbb_60;
   int lj1_isXbb_70;
   int lj1_isXbb_80;
   float lj1_D2;
   float lj1_Ntrk;

   float lj2_mass;
   float lj2_mt;
   float lj2_pt;
   int lj2_truthlabel;
   float lj2_truthmass;
   float lj2_eta;
   float lj2_phi;
   float lj2_truthpt;
   int lj2_nVRjets;
   int lj2_VRj1_id;
   int lj2_VRj2_id;
   int lj2_VRj3_id;
   float lj2_XbbDiscriminant_f25;
   int lj2_isXbb_50;
   int lj2_isXbb_60;
   int lj2_isXbb_70;
   int lj2_isXbb_80;
   float lj2_D2;
   float lj2_Ntrk;

   int NlargeRjets;
   int lj1_passWD2_50;
   int lj1_passWD2_80;
   int lj1_passWNtrk_50;
   int lj1_passWNtrk_80;
   int lj1_passWMassCut_50;
   int lj1_passWMassCut_80;
   int lj1_isWTagged_50;
   int lj1_isWTagged_80;

   int lj2_passWD2_50;
   int lj2_passWD2_80;
   int lj2_passWNtrk_50;
   int lj2_passWNtrk_80;
   int lj2_passWMassCut_50;
   int lj2_passWMassCut_80;
   int lj2_isWTagged_50;
   int lj2_isWTagged_80;

   int lj1_passZD2_50;
   int lj1_passZD2_80;
   int lj1_passZNtrk_50;
   int lj1_passZNtrk_80;
   int lj1_passZMassCut_50;
   int lj1_passZMassCut_80;
   int lj1_isZTagged_50;
   int lj1_isZTagged_80;

   int lj2_passZD2_50;
   int lj2_passZD2_80;
   int lj2_passZNtrk_50;
   int lj2_passZNtrk_80;
   int lj2_passZMassCut_50;
   int lj2_passZMassCut_80;
   int lj2_isZTagged_50;
   int lj2_isZTagged_80;

   float ptAsy;
   float dy;


   TTree *V50;//("V50","tree with V tag 50");
   TTree *V80;//("V80","tree with V tag 80");

   float v_50_mass;
   float v_50_pt;
   int v_50_truthlabel;
   float v_50_truthmass;
   float v_50_truthpt;

   float v_80_mass;
   float v_80_pt;
   int v_80_truthlabel;
   float v_80_truthmass;
   float v_80_truthpt;
   
   float MCWeight;
   float SampleWeight; //mc_weighted/dsid_weights
   float PUWeight;
   float mc_weighted; //xs*filtereff
   float dsid_weights; //sum of weight
   float tot_weight; //MCWeight*SampleWeight*PUWeight
   float lumi;
   
   bool selected_Zcand;
 
   std::vector<TH1*> histlist;
   std::vector<TH2*> histlist2;

   Nominal(TTree * /*tree*/ =0) { }
   virtual ~Nominal() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate(TH1D* hcutflow,TString mc_filename);

   TH1D * makeHist(TString name,int nbins,double x1,double x2);
   TH2D * makeHist2(TString name, int xnbins, double x1, double x2, int ynbins, double y1, double y2);
   void FatToTrackLinks(std::vector<fatjet> & largeRjets,std::vector<trackjet> & trackjets, int & nlargeRjets);
   void EraseUnselectedFatjets(std::vector<fatjet> & largeRjets);
   void MuonInJetCorrection(fatjet* largeRjet, trackjet* trackjet, std::vector<muon> & muons);
   ClassDef(Nominal,0);

};

#endif

#ifdef Nominal_cxx
void Nominal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).
   fReader.SetTree(tree);
   
}

Bool_t Nominal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef Nominal_cxx
